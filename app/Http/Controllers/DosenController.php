<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use Illuminate\Http\Request;

class DosenController extends Controller
{
    public function index()
    {
        $dosens = Dosen::latest()->simplePaginate(5);

        return view('dosen.index',compact('dosens'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
 
    public function create()
    {
        return view('dosen.create');
    }
 
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'nidn' => 'required',
            'alamat' => 'required',
            'kontak' => 'required'
        ]);
 
        Dosen::create($request->all());
 
        return redirect()->route('dosen.index')
                        ->with('success','Dosen created successfully.');
    }
 
    public function show(Dosen $dosen)
    {
        return view('dosen.show',compact('dosen'));
    }
 
    public function edit(Dosen $dosen)
    {
        return view('dosen.edit',compact('dosen'));
    }
 
    public function update(Request $request, Dosen $dosen)
    {
        $request->validate([
            'nama' => 'required',
            'nidn' => 'required',
            'alamat' => 'required',
            'kontak' => 'required'
        ]);
 
        $dosen->update($request->all());
 
        return redirect()->route('dosen.index')
                        ->with('success','Dosen updated successfully');
    }
 
    public function destroy(Dosen $dosen)
    {
        $dosen->delete();
 
        return redirect()->route('dosen.index')
                        ->with('success','Dosen deleted successfully');

    }
}
