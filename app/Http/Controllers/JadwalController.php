<?php

namespace App\Http\Controllers;

use App\Models\Jadwal;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    public function index()
    {
        $jadwals = Jadwal::latest()->simplePaginate(5);
 
        return view('jadwal.index',compact('jadwals'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
 
    public function create()
    {
        return view('jadwal.create');
    }
 
    public function store(Request $request)
    {
        $request->validate([
            'mahasiswa_id' => 'required',
            'dosen_id' => 'required',
            'judul' => 'required',
            'deskripsi' => 'required',
            'awal' => 'required',
            'akhir' => 'required',
        ]);
 
        Jadwal::create($request->all());
 
        return redirect()->route('jadwal.index')
                        ->with('success','Jadwal created successfully.');
    }
 
    public function show(Jadwal $jadwal)
    {
        return view('jadwal.show',compact('jadwal'));
    }
 
    public function edit(Jadwal $jadwal)
    {
        return view('jadwal.edit',compact('jadwal'));
    }
 
    public function update(Request $request, Jadwal $jadwal)
    {
        $request->validate([
            'mahasiswa_id' => 'required',
            'dosen_id' => 'required',
            'judul' => 'required',
            'deskripsi' => 'required',
            'awal' => 'required',
            'akhir' => 'required',
        ]);
 
        $jadwal->update($request->all());
 
        return redirect()->route('jadwal.index')
                        ->with('success','Jadwal updated successfully');
    }
 
    public function destroy(Jadwal $jadwal)
    {
        $jadwal->delete();
 
        return redirect()->route('jadwal.index')
                        ->with('success','Jadwal deleted successfully');

    }
}
