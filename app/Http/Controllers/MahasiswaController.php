<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function index()
    {
        $mahasiswas = Mahasiswa::latest()->simplePaginate(5);
 
        return view('mahasiswa.index',compact('mahasiswas'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
 
    public function create()
    {
        return view('mahasiswa.create');
    }
 
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'nim' => 'required',
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'tahun_masuk' => 'required',
        ]);
 
        Mahasiswa::create($request->all());
 
        return redirect()->route('mahasiswa.index')
                        ->with('success','Mahasiswa created successfully.');
    }
 
    public function show(Mahasiswa $mahasiswa)
    {
        return view('mahasiswa.show',compact('mahasiswa'));
    }
 
    public function edit(Mahasiswa $mahasiswa)
    {
        return view('mahasiswa.edit',compact('mahasiswa'));
    }
 
    public function update(Request $request, Mahasiswa $mahasiswa)
    {
        $request->validate([
            'nama' => 'required',
            'nim' => 'required',
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'tahun_masuk' => 'required',
        ]);
 
        $mahasiswa->update($request->all());
 
        return redirect()->route('mahasiswa.index')
                        ->with('success','Mahasiswa updated successfully');
    }
 
    public function destroy(Mahasiswa $mahasiswa)
    {
        $mahasiswa->delete();
 
        return redirect()->route('mahasiswa.index')
                        ->with('success','Mahasiswa deleted successfully');

    }
}
