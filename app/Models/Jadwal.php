<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    use HasFactory;

    protected $fillable = [
        'mahasiswa_id',
        'dosen_id',
        'judul',
        'deskripsi',
        'awal',
        'akhir',
    ];

    public function getDosen()
    {
        return $this->hasOne('App\Models\Dosen', 'id', 'dosen_id');
    }

    public function getMahasiswa()
    {
        return $this->hasOne('App\Models\Mahasiswa', 'id', 'mahasiswa_id');
    }
}
