<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
    	for($i = 1; $i <= 5; $i++){
    		DB::table('mahasiswas')->insert([
    			'nama' => $faker->name,
    			'nim' => $faker->randomNumber($nbDigits = 5, $strict = false),
    			'tanggal_lahir' => $faker->date($format = 'Y-m-d', $max = 'now'),
    			'alamat' => $faker->address,
    			'tahun_masuk' => $faker->date($format = 'Y', $max = 'now'),
    		]);

            DB::table('dosens')->insert([
    			'nama' => $faker->name,
    			'nidn' => $faker->randomNumber($nbDigits = 5, $strict = false),
    			'alamat' => $faker->address,
    			'kontak' => $faker->phoneNumber,
    		]);

			DB::table('jadwals')->insert([
    			'mahasiswa_id' => 1,
    			'dosen_id' => 1,
    			'judul' => $faker->text($maxNbChars = 30),
    			'deskripsi' => $faker->text($maxNbChars = 100),
    			'awal' => $faker->dateTime(),
    			'akhir' => $faker->dateTime(),
    		]);
    	}
    }
}
