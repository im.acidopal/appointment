@extends('app')
 
@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Create Jadwal</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('jadwal.index') }}"> Back</a>
        </div>
    </div>
</div>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<form action="{{ route('jadwal.store') }}" method="POST">
    @csrf
 
     <div class="row">
     <div class="col-xs-12 col-sm-12 col-md-12">
     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Mahasiswa:</strong>
                <input type="text" name="mahasiswa_id"  class="form-control" placeholder="Mahasiswa ID">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Dosen:</strong>
                <input type="text" name="dosen_id" class="form-control" placeholder="Dosen ID">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Judul:</strong>
                <input type="text" name="judul"  class="form-control" placeholder="Judul">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Deskripsi:</strong>
                <textarea class="form-control" style="height:150px" name="deskripsi" placeholder="Deskripsi"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Awal:</strong>
                <input type="text" name="awal" class="form-control" placeholder="2021-03-22 21:29:11">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Akhir:</strong>
                <input type="text" name="akhir" class="form-control" placeholder="2021-03-22 21:29:11">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
 
</form>
@endsection
