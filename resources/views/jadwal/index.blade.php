@extends('app')
 
@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Jadwal</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-success" href="{{ route('jadwal.create') }}"> Create Jadwal</a>
            </div>
        </div>
    </div>
 
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
 
    <table class="table table-bordered">
        <tr>
            <th width="20px" class="text-center">No</th>
            <th>Nama</th>
            <th>Dosen</th>
            <th>Judul</th>
            <th>Deskripsi</th>
            <th>Awal</th>
            <th>Akhir</th>
            <th width="280px"class="text-center">Action</th>
        </tr>
        @foreach ($jadwals as $jadwal)
        <tr>
            <td class="text-center">{{ ++$i }}</td>
            <td>{{ $jadwal->getMahasiswa->nama }}</td>
            <td>{{ $jadwal->getDosen->nama }}</td>
            <td>{{ $jadwal->judul }}</td>
            <td>{{ $jadwal->deskripsi }}</td>
            <td>{{ $jadwal->awal }}</td>
            <td>{{ $jadwal->akhir }}</td>
            <td class="text-center">
                <form action="{{ route('jadwal.destroy',$jadwal->id) }}" method="POST">
 
                    <a class="btn btn-info btn-sm" href="{{ route('jadwal.show',$jadwal->id) }}">Show</a>
 
                    <a class="btn btn-primary btn-sm" href="{{ route('jadwal.edit',$jadwal->id) }}">Edit</a>
 
                    @csrf
                    @method('DELETE')
 
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
 
    {!! $jadwals->links() !!}
 
@endsection
